-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 178.238.224.216:3307
-- Généré le : sam. 14 mars 2020 à 13:00
-- Version du serveur :  5.7.29
-- Version de PHP : 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `groupe`
--

-- --------------------------------------------------------

--
-- Structure de la table tg_adresse
--

CREATE TABLE tg_adresse (
  `ID` bigint(20) NOT NULL,
  `LB_CODE_POSTAL` varchar(12) DEFAULT NULL,
  `LB_COMMUNE` varchar(128) DEFAULT NULL,
  `LB_RUE` varchar(128) DEFAULT NULL,
  `GROUPE_ID` bigint(20) DEFAULT NULL,
  `ENTREPRISE_ID` bigint(20) DEFAULT NULL,
  `AGENCE_ID` bigint(20) DEFAULT NULL,
  `PAYS_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_adresse
--

INSERT INTO tg_adresse (`ID`, `LB_CODE_POSTAL`, `LB_COMMUNE`, `LB_RUE`, `GROUPE_ID`, `ENTREPRISE_ID`, `AGENCE_ID`, `PAYS_ID`) VALUES
(1, '35000', 'RENNES', '7 Recteur Paul Henry', 1, 1, 1, 1),
(2, '29000', 'PARIS', 'Recteur Du Paris', 1, 1, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table tg_agence
--

CREATE TABLE tg_agence (
  `ID` bigint(20) NOT NULL,
  `LOGIN_CREATOR` varchar(255) DEFAULT NULL,
  `ANNEE_AGENCE` bigint(20) DEFAULT NULL,
  `ID_INITIAL` bigint(20) DEFAULT NULL,
  `STATUT` bigint(20) DEFAULT NULL,
  `DT_CREATE` datetime NOT NULL,
  `DT_MODIF` datetime DEFAULT NULL,
  `ENTREPRISE_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_agence
--

INSERT INTO tg_agence (`ID`, `LOGIN_CREATOR`, `ANNEE_AGENCE`, `ID_INITIAL`, `STATUT`, `DT_CREATE`, `DT_MODIF`, `ENTREPRISE_ID`) VALUES
(1, 'oury', 2020, 144441, 1, '2019-06-10 14:46:12', '2019-06-10 14:46:12', 1),
(2, 'oury', 2020, 111551, 1, '2019-06-10 14:46:12', '2019-06-10 14:46:12', 1);

-- --------------------------------------------------------

--
-- Structure de la table tg_book
--

CREATE TABLE tg_book (
  `ID_BOOK` bigint(20) NOT NULL,
  `AUTHOR` varchar(255) DEFAULT NULL,
  `PUBLISHED_DATE` datetime DEFAULT NULL,
  `PUBLISHER` varchar(255) DEFAULT NULL,
  `BOOK_TITLE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_book
--

INSERT INTO tg_book (`ID_BOOK`, `AUTHOR`, `PUBLISHED_DATE`, `PUBLISHER`, `BOOK_TITLE`) VALUES
(1, 'Author Maths', '2019-06-10 14:46:12', 'publisher', 'Titre Math'),
(2, 'Author Info M', '2019-06-11 16:12:05', 'publisher', 'Titre Info');

-- --------------------------------------------------------

--
-- Structure de la table tg_calls
--

CREATE TABLE tg_calls (
  `ID` bigint(20) NOT NULL,
  `CALL_ID` varchar(255) DEFAULT NULL,
  `LABEL` varchar(255) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `CONTACT_NUMBER` varchar(255) DEFAULT NULL,
  `RECEPTION_NUMBER` varchar(255) DEFAULT NULL,
  `DURATION` bigint(20) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_calls
--

INSERT INTO tg_calls (`ID`, `CALL_ID`, `LABEL`, `TYPE`, `CONTACT_NUMBER`, `RECEPTION_NUMBER`, `DURATION`, `START_DATE`, `END_DATE`) VALUES
(1, 'abcdea', 'FR_06726072', 'S', '336726071', '336726072', 4558, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(2, 'abcdeb', 'FR_06726072', 'O', '336726072', '336726072', 5, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(3, 'abcdec', 'FR_06726072', 'E', '336726073', '336726072', 0, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(4, 'abcded', 'FR_06726072', 'S', '336726074', '336726072', 9500, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(5, 'abcdee', 'FR_06726072', 'R', '336726075', '336726072', 8, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(6, 'abcdef', 'FR_06726050', 'S', '336726071', '336726050', 4558, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(7, 'abcdeg', 'FR_06726050', 'R', '336726072', '336726050', 1, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(8, 'abcdeh', 'FR_06726050', 'E', '336726073', '336726050', 0, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(9, 'abcdei', 'FR_06726050', 'S', '336726074', '336726050', 9510, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(10, 'abcdej', 'FR_06726050', 'R', '336726075', '336726050', 6, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(11, 'abcdek', 'FR_06726072', 'S', '336726071', '336726072', 4558, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(12, 'abcdel', 'FR_06726072', 'O', '336726072', '336726072', 5, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(13, 'abcdem', 'FR_06726072', 'E', '336726073', '336726072', 0, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(14, 'abcden', 'FR_06726072', 'S', '336726074', '336726072', 9590, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(15, 'abcdeo', 'FR_06726072', 'R', '336726075', '336726072', 9, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(16, 'abcdep', 'FR_06726050', 'S', '336726071', '336726050', 4858, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(17, 'abcdeq', 'FR_06726050', 'R', '336726072', '336726050', 2, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(18, 'abcder', 'FR_06726050', 'E', '336726073', '336726050', 0, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(19, 'abcdes', 'FR_06726050', 'S', '336726074', '336726050', 910, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(20, 'abcdet', 'FR_06726050', 'R', '336726075', '336726050', 7, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(21, 'abcdeu', 'FR_06726050', 'R', '336726075', '336726050', 6, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(22, 'abcdev', 'FR_06726072', 'S', '336726071', '336726072', 4558, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(23, 'abcdew', 'FR_06726072', 'O', '336726072', '336726072', 5, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(24, 'abcdex', 'FR_06726072', 'E', '336726073', '336726072', 0, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(25, 'abcdey', 'FR_06726072', 'S', '336726074', '336726072', 9590, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(26, 'abcdez', 'FR_06726072', 'R', '336726075', '336726072', 9, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(27, 'abcdab', 'FR_06726050', 'S', '336726071', '336726050', 4858, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(28, 'abcdab', 'FR_06726050', 'R', '336726072', '336726050', 2, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(29, 'abcdac', 'FR_06726050', 'E', '336726073', '336726050', 0, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(30, 'abcdad', 'FR_06726050', 'S', '336726074', '336726050', 910, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(31, 'abcdae', 'FR_06726050', 'R', '336726075', '336726050', 7, '2020-01-25 10:15:05', '2020-01-25 10:15:05'),
(32, 'abcdaf', 'FR_06726050', 'R', '336726075', '336726050', 0, '2020-01-25 10:15:05', '2020-01-25 10:15:05');

-- --------------------------------------------------------

--
-- Structure de la table tg_contact
--

CREATE TABLE tg_contact (
  `ID` bigint(20) NOT NULL,
  `CALL_ID` varchar(255) DEFAULT NULL,
  `LABEL` varchar(255) DEFAULT NULL,
  `CONTACT_NUMBER` varchar(255) DEFAULT NULL,
  `RECEPTION_NUMBER` varchar(255) DEFAULT NULL,
  `DURATION` bigint(20) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table tg_entreprise
--

CREATE TABLE tg_entreprise (
  `ID` bigint(20) NOT NULL,
  `ENTREPRISE_NUMBER` bigint(20) DEFAULT NULL,
  `ID_NAT_ENTR` varchar(255) DEFAULT NULL,
  `ENTREPRISE_NAME` varchar(255) DEFAULT NULL,
  `LOGIN_CREATOR` varchar(255) DEFAULT NULL,
  `ANNEE_ENTREPRISE` bigint(20) DEFAULT NULL,
  `ENTREPRISE_STATUS` bigint(20) DEFAULT NULL,
  `CREATION_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `CLOSING_DATE` datetime DEFAULT NULL,
  `IMAGE_ENT` varchar(255) DEFAULT NULL,
  `GROUPE_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_entreprise
--

INSERT INTO tg_entreprise (`ID`, `ENTREPRISE_NUMBER`, `ID_NAT_ENTR`, `ENTREPRISE_NAME`, `LOGIN_CREATOR`, `ANNEE_ENTREPRISE`, `ENTREPRISE_STATUS`, `CREATION_DATE`, `UPDATE_DATE`, `CLOSING_DATE`, `IMAGE_ENT`, `GROUPE_ID`) VALUES
(1, 1, 'ID_NAT_ENT1', 'Ent Alyotech', 'oury', 2020, 1, '2019-07-10 14:46:12', '2019-07-10 14:46:12', '2019-07-05 14:46:12', 'assets/img/France.png', 1),
(2, 2, 'ID_NAT_ENT2', 'Ent Eurogiciel', 'oury', 2020, 1, '2019-10-10 14:46:12', '2019-10-10 14:46:12', '2019-10-05 14:46:12', 'assets/img/France.png', 1),
(3, 3, 'ID_NAT_ENT3', 'Ent Eurogiciel', 'ourysow', 2020, 1, '2019-11-10 14:46:12', '2019-11-10 14:46:12', '2019-11-05 14:46:12', 'assets/img/France.png', 1),
(4, 4, 'ID_NAT_ENT4', 'Ent Hightech RE', 'oury', 2020, 1, '2019-12-10 14:46:12', '2019-12-10 14:46:12', '2019-12-05 14:46:12', 'assets/img/France.png', 1),
(5, 5, 'ID_NAT_ENT5', 'Ent Euro 2', 'oury', 2020, 1, '2020-01-01 14:46:12', '2020-01-01 14:46:12', '2020-01-01 14:46:12', 'assets/img/France.png', 1),
(6, 6, 'ID_NAT_ENT6', 'Ent Eurog 3', 'ourysow', 2020, 1, '2020-01-02 14:46:12', '2020-01-02 14:46:12', '2020-01-02 14:46:12', 'assets/img/France.png', 1),
(7, 7, 'ID_NAT_ENT5', 'Ent Euro 3', 'oury', 2020, 1, '2020-01-03 14:46:12', '2020-01-03 14:46:12', '2020-01-03 14:46:12', 'assets/img/France.png', 1),
(8, 8, 'ID_NAT_ENT6', 'Ent Eurog 4', 'ourysow', 2020, 1, '2020-01-04 14:46:12', '2020-01-04 14:46:12', '2020-01-04 14:46:12', 'assets/img/France.png', 1),
(9, 9, 'ID_NAT_ENT1', 'Ent Alyotech', 'oury', 2020, 1, '2019-07-10 14:46:12', '2019-07-10 14:46:12', '2019-07-05 14:46:12', 'assets/img/France.png', 2),
(10, 10, 'ID_NAT_ENT2', 'Ent Eurogiciel', 'oury', 2020, 1, '2019-10-10 14:46:12', '2019-10-10 14:46:12', '2019-10-05 14:46:12', 'assets/img/France.png', 2);

-- --------------------------------------------------------

--
-- Structure de la table tg_groupe
--

CREATE TABLE tg_groupe (
  `ID` bigint(20) NOT NULL,
  `GROUPE_NUMBER` bigint(20) DEFAULT NULL,
  `ID_NAT_GROUPE` varchar(255) DEFAULT NULL,
  `GROUPE_NAME` varchar(255) DEFAULT NULL,
  `GROUPE_STATUS` bigint(20) DEFAULT NULL,
  `LOGIN_CREATOR` varchar(255) DEFAULT NULL,
  `ANNEE_GROUPE` bigint(20) DEFAULT NULL,
  `CREATION_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `CLOSING_DATE` datetime DEFAULT NULL,
  `IMAGE_GROUPE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_groupe
--

INSERT INTO tg_groupe (`ID`, `GROUPE_NUMBER`, `ID_NAT_GROUPE`, `GROUPE_NAME`, `GROUPE_STATUS`, `LOGIN_CREATOR`, `ANNEE_GROUPE`, `CREATION_DATE`, `UPDATE_DATE`, `CLOSING_DATE`, `IMAGE_GROUPE`) VALUES
(1, 1, 'ID_NAT_GGE1', 'Groupe Scalian', 1, 'oury', 2020, '2019-12-24 14:46:12', '2019-12-24 14:46:12', '2019-06-10 14:46:12', 'assets/img/France.png'),
(2, 2, 'ID_NAT_GGE2', 'Groupe Capge F', 1, 'oury', 2020, '2019-12-22 14:46:12', '2019-12-23 14:46:12', '2019-06-10 14:46:12', 'assets/img/USA.svg'),
(3, 3, 'ID_NAT_GGE3', 'Groupe Atos F', 1, 'oury', 2020, '2019-12-15 14:46:12', '2019-12-15 14:46:12', '2019-06-10 14:46:12', 'assets/img/France.png'),
(4, 4, 'ID_NAT_GGE4', 'Groupe Orange', 1, 'oury', 2020, '2019-12-10 14:46:12', '2019-12-12 14:46:12', '2019-06-10 14:46:12', 'assets/img/USA.svg'),
(5, 5, 'ID_NAT_GGE5', 'Groupe CAS FR', 1, 'oury', 2020, '2019-12-08 14:46:12', '2019-12-10 14:46:12', '2019-06-10 14:46:12', 'assets/img/France.png'),
(6, 6, 'ID_NAT_GGE6', 'Groupe OBS FR', 1, 'oury', 2020, '2019-12-05 14:46:12', '2019-12-08 14:46:12', '2019-06-10 14:46:12', 'assets/img/Russia.svg'),
(7, 7, 'ID_NAT_GGE7', 'Groupe Lcl FR', 1, 'oury', 2020, '2019-12-02 14:46:12', '2019-12-03 14:46:12', '2019-06-10 14:46:12', 'assets/img/China.svg'),
(8, 8, 'ID_NAT_GGE8', 'Groupe Macdos', 1, 'ourysow', 2020, '2019-06-05 14:46:12', '2019-06-10 14:46:12', '2019-06-10 14:46:12', 'assets/img/USA.svg'),
(9, 9, 'ID_NAT_GGF1', 'Groupe Scalian', 1, 'oury', 2020, '2019-12-24 14:46:12', '2019-12-24 14:46:12', '2019-06-10 14:46:12', 'assets/img/France.png'),
(10, 10, 'ID_NAT_GGF2', 'Groupe Atos P', 1, 'oury', 2020, '2019-12-22 14:46:12', '2019-12-23 14:46:12', '2019-06-10 14:46:12', 'assets/img/USA.svg'),
(11, 11, 'ID_NAT_GGF3', 'Groupe OBS RE', 1, 'oury', 2020, '2019-12-15 14:46:12', '2019-12-15 14:46:12', '2019-06-10 14:46:12', 'assets/img/France.png'),
(12, 12, 'ID_NAT_GGF4', 'Groupe OBS', 1, 'oury', 2020, '2019-12-10 14:46:12', '2019-12-12 14:46:12', '2019-06-10 14:46:12', 'assets/img/USA.svg'),
(13, 13, 'SCA_NAT_G13', 'Groupe Alyo 17', 1, 'oury', 2020, '2019-12-24 14:46:12', '2019-12-24 14:46:12', '2019-06-10 14:46:12', 'assets/img/France.png'),
(14, 14, 'SCA_NAT_G14', 'Groupe Capge 17', 1, 'oury', 2020, '2019-12-22 14:46:12', '2019-12-23 14:46:12', '2019-06-10 14:46:12', 'assets/img/USA.svg'),
(15, 15, 'SCA_NAT_G15', 'Groupe Atos F', 1, 'oury', 2020, '2019-12-15 14:46:12', '2019-12-15 14:46:12', '2019-06-10 14:46:12', 'assets/img/France.png'),
(16, 16, 'SCA_NAT_G16', 'Groupe Fanta 10', 1, 'oury', 2020, '2019-12-10 14:46:12', '2019-12-12 14:46:12', '2019-06-10 14:46:12', 'assets/img/USA.svg'),
(17, 17, 'SCA_NAT_G17', 'GPE CAS Agri', 1, 'oury', 2020, '2019-12-08 14:46:12', '2019-12-10 14:46:12', '2019-06-10 14:46:12', 'assets/img/France.png'),
(18, 18, 'SCA_NAT_G18', 'GPE OBS Orange', 1, 'oury', 2020, '2019-12-05 14:46:12', '2019-12-08 14:46:12', '2019-06-10 14:46:12', 'assets/img/Russia.svg'),
(19, 19, 'SCA_NAT_G19', 'GPE Lcl Credi', 1, 'oury', 2020, '2019-12-02 14:46:12', '2019-12-03 14:46:12', '2019-06-10 14:46:12', 'assets/img/China.svg'),
(20, 20, 'SCA_NAT_G20', 'GPE Macdos RE', 1, 'ourysow', 2020, '2019-06-05 14:46:12', '2019-06-10 14:46:12', '2019-06-10 14:46:12', 'assets/img/USA.svg'),
(21, 21, 'SCA_NAT_G21', 'GPE Scalian 21', 1, 'oury', 2020, '2019-12-24 14:46:12', '2019-12-24 14:46:12', '2019-06-10 14:46:12', 'assets/img/France.png'),
(22, 22, 'SCA_NAT_G22', 'GPE Atos OB', 1, 'oury', 2020, '2019-12-22 14:46:12', '2019-12-23 14:46:12', '2019-06-10 14:46:12', 'assets/img/USA.svg'),
(23, 23, 'SCA_NAT_G23', 'GPE OBS PAR', 1, 'oury', 2020, '2019-12-15 14:46:12', '2019-12-15 14:46:12', '2019-06-10 14:46:12', 'assets/img/France.png'),
(24, 24, 'SCA_NAT_G24', 'GPE ATOS P', 1, 'oury', 2020, '2019-12-10 14:46:12', '2019-12-12 14:46:12', '2019-06-10 14:46:12', 'assets/img/USA.svg');

-- --------------------------------------------------------

--
-- Structure de la table tg_marque
--

CREATE TABLE tg_marque (
  `ID` bigint(20) NOT NULL,
  `DT_CREATION` datetime DEFAULT NULL,
  `DT_MODIF` datetime DEFAULT NULL,
  `LB_MARQUE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_marque
--

INSERT INTO tg_marque (`ID`, `DT_CREATION`, `DT_MODIF`, `LB_MARQUE`) VALUES
(1, '2019-12-24 14:46:12', '2019-12-24 14:46:12', 'PC Informatique');

-- --------------------------------------------------------

--
-- Structure de la table tg_operateur
--

CREATE TABLE tg_operateur (
  `ID` bigint(20) NOT NULL,
  `LOGIN_OPERATEUR` varchar(255) DEFAULT NULL,
  `NOM_OPERATEUR` varchar(255) DEFAULT NULL,
  `GROUPE_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_operateur
--

INSERT INTO tg_operateur (`ID`, `LOGIN_OPERATEUR`, `NOM_OPERATEUR`, `GROUPE_ID`) VALUES
(1, 'oury', 'Oury Sow', 1),
(2, 'oury', 'Oury Sow', 2),
(3, 'oury', 'Oury Sow', 3),
(4, 'oury', 'Oury Sow', 4),
(5, 'oury', 'Oury Sow', 5);

-- --------------------------------------------------------

--
-- Structure de la table tg_pays
--

CREATE TABLE tg_pays (
  `ID` bigint(20) NOT NULL,
  `CD_PAYS` varchar(3) NOT NULL,
  `LB_PAYS` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_pays
--

INSERT INTO tg_pays (`ID`, `CD_PAYS`, `LB_PAYS`) VALUES
(1, 'FR', 'FRANCE'),
(2, 'US', 'ETATS USA'),
(3, 'GN', 'GUINEE');

-- --------------------------------------------------------

--
-- Structure de la table tg_privilege
--

CREATE TABLE tg_privilege (
  `ID` bigint(20) NOT NULL,
  `PRIVILEGE_LABEL` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_privilege
--

INSERT INTO tg_privilege (`ID`, `PRIVILEGE_LABEL`) VALUES
(1, 'WRITE_PRIVILEGE'),
(2, 'READ_PRIVILEGE');

-- --------------------------------------------------------

--
-- Structure de la table tg_produit
--

CREATE TABLE tg_produit (
  `ID` bigint(20) NOT NULL,
  `REF_PRODUIT` varchar(255) DEFAULT NULL,
  `LB_PRODUIT` varchar(255) DEFAULT NULL,
  `STATUS` bigint(20) DEFAULT NULL,
  `CAT_PRODUIT` varchar(255) DEFAULT NULL,
  `QTE_PRODUIT` bigint(20) DEFAULT NULL,
  `PRIX_UNIT` double DEFAULT NULL,
  `DT_CREATE` datetime DEFAULT NULL,
  `DT_UPDATE` datetime DEFAULT NULL,
  `DESC_PRODUIT` varchar(255) DEFAULT NULL,
  `AGENCE_ID` bigint(20) DEFAULT NULL,
  `MARQUE_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_produit
--

INSERT INTO tg_produit (`ID`, `REF_PRODUIT`, `LB_PRODUIT`, `STATUS`, `CAT_PRODUIT`, `QTE_PRODUIT`, `PRIX_UNIT`, `DT_CREATE`, `DT_UPDATE`, `DESC_PRODUIT`, `AGENCE_ID`, `MARQUE_ID`) VALUES
(1, 'REF_P1', 'PC HP Informatique', 1, 'HIGTECH', 25, 250, '2019-06-10 14:46:12', '2019-06-10 14:46:12', 'PC HP Informatique', 1, 1),
(2, 'REF_P2', 'PC HP Informatique', 1, 'HIGTECH', 25, 350, '2019-06-10 14:46:12', '2019-06-10 14:46:12', 'PC HP Informatique', 1, 1),
(3, 'REF_P3', 'PC HP Informatique', 1, 'HIGTECH', 25, 250, '2019-06-10 14:46:12', '2019-06-10 14:46:12', 'PC HP Informatique', 1, 1),
(4, 'REF_P4', 'PC HP Informatique', 1, 'HIGTECH', 25, 150, '2019-06-10 14:46:12', '2019-06-10 14:46:12', 'PC HP Informatique', 1, 1),
(5, 'REF_P5', 'PC HP Informatique', 1, 'HIGTECH', 25, 230, '2019-06-10 14:46:12', '2019-06-10 14:46:12', 'PC HP Informatique', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table tg_region
--

CREATE TABLE tg_region (
  `ID` bigint(20) NOT NULL,
  `CD_REGION` varchar(3) NOT NULL,
  `LB_REGION` varchar(32) NOT NULL,
  `PAYS_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_region
--

INSERT INTO tg_region (`ID`, `CD_REGION`, `LB_REGION`, `PAYS_ID`) VALUES
(1, 'BRE', 'Brétagne', 1),
(2, 'IFR', 'Paris', 1),
(3, 'ALP', 'Caen', 1);

-- --------------------------------------------------------

--
-- Structure de la table tg_role
--

CREATE TABLE tg_role (
  `ID` bigint(20) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LB_ROLE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_role
--

INSERT INTO tg_role (`ID`, `DESCRIPTION`, `LB_ROLE`) VALUES
(1, 'Admin', 'ROLE_ADMIN'),
(2, 'User Simple', 'ROLE_USER'),
(3, 'Invité', 'ROLE_INVITE');

-- --------------------------------------------------------

--
-- Structure de la table tg_roles_privileges
--

CREATE TABLE tg_roles_privileges (
  `ROLES_ID` bigint(20) NOT NULL,
  `PRIVILEGES_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_roles_privileges
--

INSERT INTO tg_roles_privileges (`ROLES_ID`, `PRIVILEGES_ID`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 2);

-- --------------------------------------------------------

--
-- Structure de la table tg_user
--

CREATE TABLE tg_user (
  `ID` bigint(20) NOT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `ENABLED` bit(1) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_user
--

INSERT INTO tg_user (`ID`, `EMAIL`, `ENABLED`, `USERNAME`, `PASSWORD`) VALUES
(1, 'oury@gmail.com', b'1', 'oury', '$2a$10$FZ7QsQhUbOUto84GvKgQw.tRFDWVjUTNA/PMna3hV09bGoh6KNKOW'),
(2, 'mamadou@yahoo.fr', b'1', 'mamadou', '$2a$10$xAjZ0exl7.6M.8eeldFcHeKxypYkmW7VTDuQTbuuOe0GvSmmFGb0u'),
(3, 'ourysow@gmail.com', b'1', 'ourysow', '$2a$10$C7i3p2N7.m4R1HTZ/jSxYOh5cCVtfb/FGizN1vOSal447bRU6JKHa'),
(4, 'mike@gmail.com', b'1', 'mike', '$2a$10$eRGAcC.DKY8w9d1U.pEs0.jU8.80p6KSoylaw0C3cux3WkkvHpjvC'),
(5, 'tidiane@gmail.com', b'1', 'tidiane', '$2a$10$1g08MQFO1Augu7pXRq0e7O81ZtR46QwORvU9hBmte.1pkmz03j59y');

-- --------------------------------------------------------

--
-- Structure de la table tg_users_roles
--

CREATE TABLE tg_users_roles (
  `USER_ID` bigint(20) NOT NULL,
  `ROLE_ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_users_roles
--

INSERT INTO tg_users_roles (`USER_ID`, `ROLE_ID`) VALUES
(1, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 3),
(5, 3);

-- --------------------------------------------------------

--
-- Structure de la table tg_city
--

CREATE TABLE tg_city (
  `ID` bigint(20) NOT NULL,
  `COUNTRY` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table tg_city
--

INSERT INTO tg_city (`ID`, `COUNTRY`, `NAME`) VALUES
(1, 'FRANCE', 'PARIS'),
(2, 'FRANCE', 'RENNES');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table tg_adresse
--
ALTER TABLE tg_adresse
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_ENTSR7PQ96ELJQE` (`ENTREPRISE_ID`),
  ADD KEY `FK_PAYSR7PQ96ELJQE` (`PAYS_ID`),
  ADD KEY `FK_AGENCEPQ96ELJQE` (`AGENCE_ID`),
  ADD KEY `FK_GROUPEPQ96ELJQE` (`GROUPE_ID`);

--
-- Index pour la table tg_agence
--
ALTER TABLE tg_agence
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_AG_8NIPAWMQTWP3JNBA` (`ENTREPRISE_ID`);

--
-- Index pour la table tg_book
--
ALTER TABLE tg_book
  ADD PRIMARY KEY (`ID_BOOK`);

--
-- Index pour la table tg_calls
--
ALTER TABLE tg_calls
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table tg_contact
--
ALTER TABLE tg_contact
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table tg_entreprise
--
ALTER TABLE tg_entreprise
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_ENT_GPjwmtytvma9e06` (`GROUPE_ID`);

--
-- Index pour la table tg_groupe
--
ALTER TABLE tg_groupe
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `UK_GROUPE_NUMBER_TG` (`GROUPE_NUMBER`);

--
-- Index pour la table tg_marque
--
ALTER TABLE tg_marque
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table tg_operateur
--
ALTER TABLE tg_operateur
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FKI9AFPNKQPOOJL6UM7AWP` (`GROUPE_ID`);

--
-- Index pour la table tg_pays
--
ALTER TABLE tg_pays
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table tg_privilege
--
ALTER TABLE tg_privilege
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table tg_produit
--
ALTER TABLE tg_produit
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1CYDSLCSM0L2WK0G` (`MARQUE_ID`),
  ADD KEY `FK_AGENCE0584L2WK0G` (`AGENCE_ID`);

--
-- Index pour la table tg_region
--
ALTER TABLE tg_region
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_PAYS6TGIX6JM8CW` (`PAYS_ID`);

--
-- Index pour la table tg_role
--
ALTER TABLE tg_role
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table tg_roles_privileges
--
ALTER TABLE tg_roles_privileges
  ADD KEY `FK_PRIVILEGESJSXTCEU3` (`PRIVILEGES_ID`),
  ADD KEY `FK_ROLE96CYJN00F3FCJ6` (`ROLES_ID`);

--
-- Index pour la table tg_user
--
ALTER TABLE tg_user
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `UK_GROUPE_NUMBER_TU` (`EMAIL`);

--
-- Index pour la table tg_users_roles
--
ALTER TABLE tg_users_roles
  ADD KEY `FK_USER2YC6RRWK1YL8Y` (`USER_ID`),
  ADD KEY `FK_ROLE2YC6RRWK1YL8Y` (`ROLE_ID`);

--
-- Index pour la table tg_city
--
ALTER TABLE tg_city
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table tg_adresse
--
ALTER TABLE tg_adresse
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table tg_agence
--
ALTER TABLE tg_agence
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table tg_book
--
ALTER TABLE tg_book
  MODIFY `ID_BOOK` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table tg_calls
--
ALTER TABLE tg_calls
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT pour la table tg_contact
--
ALTER TABLE tg_contact
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table tg_entreprise
--
ALTER TABLE tg_entreprise
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table tg_groupe
--
ALTER TABLE tg_groupe
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table tg_marque
--
ALTER TABLE tg_marque
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table tg_operateur
--
ALTER TABLE tg_operateur
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table tg_pays
--
ALTER TABLE tg_pays
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table tg_privilege
--
ALTER TABLE tg_privilege
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table tg_produit
--
ALTER TABLE tg_produit
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table tg_region
--
ALTER TABLE tg_region
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table tg_role
--
ALTER TABLE tg_role
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table tg_user
--
ALTER TABLE tg_user
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table tg_city
--
ALTER TABLE tg_city
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table tg_adresse
--
ALTER TABLE tg_adresse
  ADD CONSTRAINT `FK_AGENCEPQ96ELJQE` FOREIGN KEY (`AGENCE_ID`) REFERENCES tg_agence (`ID`),
  ADD CONSTRAINT `FK_ENTSR7PQ96ELJQE` FOREIGN KEY (`ENTREPRISE_ID`) REFERENCES tg_entreprise (`ID`),
  ADD CONSTRAINT `FK_GROUPEPQ96ELJQE` FOREIGN KEY (`GROUPE_ID`) REFERENCES tg_groupe (`ID`),
  ADD CONSTRAINT `FK_PAYSR7PQ96ELJQE` FOREIGN KEY (`PAYS_ID`) REFERENCES tg_pays (`ID`);

--
-- Contraintes pour la table tg_agence
--
ALTER TABLE tg_agence
  ADD CONSTRAINT `FK_AG_8NIPAWMQTWP3JNBA` FOREIGN KEY (`ENTREPRISE_ID`) REFERENCES tg_entreprise (`ID`);

--
-- Contraintes pour la table tg_entreprise
--
ALTER TABLE tg_entreprise
  ADD CONSTRAINT `FK_ENT_GPjwmtytvma9e06` FOREIGN KEY (`GROUPE_ID`) REFERENCES tg_groupe (`ID`);

--
-- Contraintes pour la table tg_operateur
--
ALTER TABLE tg_operateur
  ADD CONSTRAINT `FKI9AFPNKQPOOJL6UM7AWP` FOREIGN KEY (`GROUPE_ID`) REFERENCES tg_groupe (`ID`);

--
-- Contraintes pour la table tg_produit
--
ALTER TABLE tg_produit
  ADD CONSTRAINT `FK1CYDSLCSM0L2WK0G` FOREIGN KEY (`MARQUE_ID`) REFERENCES tg_marque (`ID`),
  ADD CONSTRAINT `FK_AGENCE0584L2WK0G` FOREIGN KEY (`AGENCE_ID`) REFERENCES tg_agence (`ID`);

--
-- Contraintes pour la table tg_region
--
ALTER TABLE tg_region
  ADD CONSTRAINT `FK_PAYS6TGIX6JM8CW` FOREIGN KEY (`PAYS_ID`) REFERENCES tg_pays (`ID`);

--
-- Contraintes pour la table tg_roles_privileges
--
ALTER TABLE tg_roles_privileges
  ADD CONSTRAINT `FK_PRIVILEGESJSXTCEU3` FOREIGN KEY (`PRIVILEGES_ID`) REFERENCES tg_privilege (`ID`),
  ADD CONSTRAINT `FK_ROLE96CYJN00F3FCJ6` FOREIGN KEY (`ROLES_ID`) REFERENCES tg_role (`ID`);

--
-- Contraintes pour la table tg_users_roles
--
ALTER TABLE tg_users_roles
  ADD CONSTRAINT `FK_ROLE2YC6RRWK1YL8Y` FOREIGN KEY (`ROLE_ID`) REFERENCES tg_role (`ID`),
  ADD CONSTRAINT `FK_USER2YC6RRWK1YL8Y` FOREIGN KEY (`USER_ID`) REFERENCES tg_user (`ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
