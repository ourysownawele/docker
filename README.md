### Commet faire du ménage dans les conteneurs et images docker ###

Si vous utilisez docker vous avez surement remarqué que l'espace disque utilisé augmente rapidement. Voici quelques règles pour économiser un peu de place sur votre disque.

Supprimer les volumes associés à un conteneur
Lorsqu'on supprime un conteneur penser à utiliser l'option -v qui permet de supprimer les volumes associés à un conteneur.

### Supprimer tous les conteneurs qui ne tournent pas:

```
 docker rm -v $(docker ps -aqf status=exited)
```
Recréé un conteneur est assez rapide du moment que son image est disponible. Ce qui nous amène vers le nettoyage des images inutiles.

### Supprimer les images inutiles
J'appelle image "inutile" une image "intermédiaire" qui sert dans la construction d'une image "finale" et qui n'est donc jamais utilisé pour créer un conteneur.

On peut supprimer ces images avec la commande suivante:

```
 docker rmi $(docker images -qf dangling=true)
```
Souvent indispensable après un docker pull.

### Supprimer les volumes orphelins
Un volume orphelin est un volume pour lequel son conteneur associé a été supprimé sans l'option -v. 
Pour supprimer ces volumes on a la commande suivante:

```
 docker volume rm $(docker volume ls -qf dangling=true)
```
### supprimer l'ensemble de l'espace occupé par des containers inutilisés.
```
alias docker-clean='docker rm $(docker ps -qa --no-trunc --filter "status=exited") && docker rmi $(docker images --filter "dangling=true" -q --no-trunc)'
```
